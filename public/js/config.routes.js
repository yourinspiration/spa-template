(function() {
	'use strict';

	var app = angular.module('app');

	// Add routes to the application.
	app.constant('routes', getRoutes());

	app.config(['$routeProvider', 'routes', routeConfigurator]);
	function routeConfigurator($routeProvider, routes) {
		routes.forEach(function(r) {
			$routeProvider.when(r.url, r.config);
		});
		$routeProvider.otherwise({ redirectTo: '/view' }); // Define a 'fallback'
	}

	// Definition of routes go here...
	function getRoutes() {
		return [
			{   // Add an object like this for each route
				url: '/view',
				config: {
        			templateUrl: 'js/view/view.html',
        			controller: 'ViewController'
      			}
      		}
		]
	}
})();
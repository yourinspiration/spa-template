(function() {
	'use strict';

	var app = angular.module('app');

	// Configuration of angular application before it starts.
	app.config(['$logProvider', function($logProvider) {
		if ($logProvider.debugEnabled) {
			$logProvider.debugEnabled(true);
		}
	}]);
})();
(function() {
  'use strict';

  // Define angular module for application.
  // Name must be the same as the data-ng-app directive.
  var app = angular.module('app', [
    'ngRoute',  // Add dependency for routing
    'ngAnimate' // Add dependency for angular animation
  ]);

  app.run(['$route', function($route) {
    
  }]);
})();
